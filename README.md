# ohmyzsh-installer

#### 介绍
install ohmyzsh now

#### 安装教程

1.  ``sh -c "$(curl -fsSL https://gitee.com/sysdl132/ohmyzsh-installer/raw/master/install.sh)"``

#### 使用说明

take a look at sysdl132/ohmyzsh (a mirror of github:ohmyzsh/ohmyzsh)


```
Copyright (C) 2020  sysdl132

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

